#include <iostream>
using namespace std;

template< typename T >
bool firstIsLess(T a, T b)
{
	return a < b;
}

void sort(double *arr, int length)
{
	for(int i = 0; firstIsLess(i, length); ++i)
	{
		double currElmt = arr[i];
		for(int j = i - 1; !firstIsLess(j, 0); --j)
		{
			if(firstIsLess(currElmt, arr[j]))
			{
				arr[j+1] = arr[j];
				arr[j] = currElmt;
			}
		}
	}
}

void printArr(double *arr, int length)
{
	for(int i = 0; i < length; ++i)
		cout << arr[i] << endl;
	
	char ch;
	cin >> ch;
}



int main(int argc, char **argv)
{
	double arr[] = {-4, 0.2, 8, 1, 4, 7, -19};
	int length = sizeof(arr) / sizeof(arr[0]);
	sort(arr, length);
	printArr(arr, length);
	return 0;
}

